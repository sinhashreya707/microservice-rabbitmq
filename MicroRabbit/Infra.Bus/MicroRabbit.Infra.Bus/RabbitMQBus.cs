﻿using System;

namespace MicroRabbit.Infra.Bus
{
    public sealed class RabbitMQBus:IEventBus
    {
         private readonly IMediator _mediator;
         private readonly Dictionary<string,List<Type>> _handlers;
         private readonly List<Type> _eventTypes;
         private readonly IServiceScopeFactory _serviceScopeFactory;

         public RabbitMQBus(IMediator mediator,IServiceScopeFactory serviceScopeFactory){
             _mediator=mediator;
             _handlers=new Dictionary<string,List<Type>>();
             _serviceScopeFactory=serviceScopeFactory;
            _events=new List<Type>();
         }

         public Task SendCommand<T>(T command) where T:Command{
                return _mediator.Send(command);
         }
         public void Publish<T>(T @event) where T:Event{
            var factory=new ConnectionFactory(){HostName="localhost"};
            using(var connection=factory.CreateConnection())
            using(var channel=connection.CreateModel()){
                var eventName=@event.GetType().Name;
                channel.QueueDeclare(eventName,false,false,false,null);

                string message=JsonConvert.SerializeObject(@event);
                var body=Encoding.UTF8.GetBytes(message);
                channel.BasicPublish("","Basic Test",null,body);
                Console.WriteLine("Send message{0}",message);

            }

         }
         public void Subscribe<T,TH>() where T:Event where TH:IEventHandler<T>{

                var eventName=typeof(T).Name;
                var handlerType=typeof(TH);
                if(!_eventTypes.Contains(typeof(T))){
                    _eventTypes.Add(typeof(T));
                }
                 if(!_handlers.Contains(eventName)){
                    _handlers.Add(eventName,new List<Type>());
                }
                if(!_handlers[eventName].Any(s=>s.GetType()==handlerType)){
                throw new ArgumentException(
                    $"Handler Type is registered for {handlerType.Name}is already registered for {eventName}";
                );
                }
                _handlers[eventName].Add(handlerType);
                StartBasicConsume<T>();
         }
         private void StartBasicConsume<T>() where T:Event{
            var factory=new ConnectionFactory(){HostName="localhost"};
            var connection=factory.CreateConnection();
            var channel=connection.CreateModel();
            var eventName=typeof(T).Name;
            channel.QueueDeclare(eventName,false,false,false,null);

            var consumer=new AsyncEventingBasicConsumer(channel);
            consumer.Received+=Consumer_Received;
            channel.BasicConsume(eventName,true,consumer);

            }
            private async Task Consumer_Received(object sender,BasicDeliverEventArgs e){
            var eventName=e.RoutingKey;
            var message=Encoding.UTF8.GetString(e.Body.ToArray());
            try{
             await ProcessEvent(eventName,message).ConfigureAwait(false);
            }
            catch(Exception ex){

            }

            }
            private async Task ProcessEvent(string eventName,string message){
            if(_handlers.ContainsKey(eventName))
            {
                using(var scope=_serviceScopeFactory.CreateScope())
                {
                var subscriptions=_handlers[eventName];
                foreach(var subscription in subscriptions){
                    //var handler=Activator.CreateInstance(subscription);
                    var handler=scope.ServiceProvider.GetService(subscription);

                    if(handler==null)continue;
                    var eventType=_eventTypes.SingleOrDefault(t=>t.Name==eventName);
                    var @event=JsonConvert.DeserializeObject(message,eventType);
                    var concreteType=typeof(IEventHandler<>).MakeGenericType(eventType);
                    await (Task)concreteType.GetMethod("Handle").Invoke(handler,new object[]{event});
                }
                }
               
            }

            }
         }

    }
}
