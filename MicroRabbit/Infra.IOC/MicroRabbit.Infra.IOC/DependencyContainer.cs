﻿using System;

namespace MicroRabbit.Infra.IOC
{
    public class DependencyContainer
    {
        public static RegisterServices(IServiceCollection services){
        //    //Domain Bus
        //     services.AddTransient<IEventBus,RabbitMQBus>();
           //Domain Bus
            services.AddSingleton<IEventBus,RabbitMQBus>(sp=>{
                var scopeFactory=sp.GetRequiredService<IServiceScopeFactory>();
              return new RabbitMQBus(sp.GetService<IMediator>(),scopeFactory);
            });
          
           //Domain Events
            services.AddTransient<IEventHandler<TransferCreatedEvent>,TransferEventHandler>();

            //Domain Subscription
            services.AddTransient<TransferEventHandler>();

           //Domain Banking Commands
            services.AddTransient<IRequestHandler<CreateTransferCommand,bool>,TransferCommandHandler>();

             //Application Services
            services.AddTransient<IAccountService,AccountService>();
            services.AddTransient<ITransferService,TransferService>();

            //Data
            services.AddTransient<IAccountRepository,AccountRepository>();
            services.AddTransient<ITransferRepository,TransferRepository>();
            services.AddTransient<BankingDbContext>();
            services.AddTransient<TransferDbContext>();

        }
    }
}
