﻿using System;
using RabbitMQ.Client;
using System.Text;
namespace Producer
{
    class Sender
    {
        static void Main(string[] args)
        {
            var factory=new ConnectionFactory(){HostName="localhost"};
            using(var connection=factory.CreateConnection())
            using(var channel=connection.CreateModel()){
                channel.QueueDeclare("Basic Test",false,false,false,null);

                string message="Getting started with dotnet core";
                var body=Encoding.UTF8.GetBytes(message);
                channel.BasicPublish("","Basic Test",null,body);
                Console.WriteLine("Send message{0}",message);

            }
                Console.WriteLine("Press[enter] to exit the sender app");
                Console.ReadLine();

        }
    }
}
