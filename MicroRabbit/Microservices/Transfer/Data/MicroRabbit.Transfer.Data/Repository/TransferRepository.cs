namespace MicroRabbit.Transfer.Data.Repository
{
    public class TransferRepository:ITransferRepository
    {
        private TransferDbContext _ctx;
        public TransferRepository(TransferDbContext ctx){
        _ctx=ctx;

        }
        public  IEnumerable<TransferLog> GetTransferLogs(){
        return _ctx.TransferLogs;
        }

    }
}