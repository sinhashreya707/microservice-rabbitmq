﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MicroRabbit.Transfer.API.Controllers
{
    [Route("api/[controller]")]

    [ApiController]
    public class TransferController : ControllerBase
    {
        
        private readonly ITransferService _transferservice;
        public TransferController(IAccountService transferservice)
        {
            _transferservice = transferservice;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TransferLog>> Get()
        {
           return Ok(_transferservice.GetTransferLogs());
        }

        // [HttpPost]
        // public IActionResult Post([FromBody]AccountTransfer accountTransfer)
        // {
        //     _accountservice.Transfer(accountTransfer);
        //    return Ok(accountTransfer);
        // }
    }
}
