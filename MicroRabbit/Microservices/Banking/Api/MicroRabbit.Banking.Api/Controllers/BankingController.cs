﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MicroRabbit.Banking.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class BankingController : ControllerBase
    {
        
        private readonly IAccountService _accountservice;
        public BankingController(IAccountService accountservice)
        {
            _accountservice = accountservice;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Account>> Get()
        {
           return Ok(_accountservice.GetAccounts());
        }

        [HttpPost]
        public IActionResult Post([FromBody]AccountTransfer accountTransfer)
        {
            _accountservice.Transfer(accountTransfer);
           return Ok(accountTransfer);
        }
    }
}
