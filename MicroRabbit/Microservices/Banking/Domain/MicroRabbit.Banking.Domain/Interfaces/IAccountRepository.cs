using System;
using System.Collections.Generic;
using System.Text;
using MicroRabbit.Banking.Domain.Models;

using MicroRabbit.Banking.Domain.Interfaces;
{
    public interface IAccountRepository
    {
         IEnumerable<Account> GetAccounts();
    }
}