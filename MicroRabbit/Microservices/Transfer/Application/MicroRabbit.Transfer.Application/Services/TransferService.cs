namespace MicroRabbit.Transfer.Application.Services
{
    public class TransferService:ITransferService
    {
        private readonly ITransferRepository _transferRepository;
        private readonly IEventBus _bus;

        public TransferService(ITransferRepository transferRepository,IEventBus bus){
            _transferRepository=transferRepository;
            _bus=bus;
        }
        public IEnumerable<TransferLog> GetTrasferLogs(){
            return _transferRepository.GetTrasferLogs();
        }

    }
}